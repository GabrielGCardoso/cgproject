#include "Object.h"

Object::Object()
{
    Red = 0;
    Green = 0;
    Blue = 0;
    type = 0;
    scaleX = 1;
    scaleY = 1;
    side = 3;
}

Object::Object(int r, int g, int b, int t, double scaleX, double scaleY, int side){
    this->Red = r;
    this->Green = g;
    this->Blue = b;
    this->type = t;
    this->scaleX = scaleX;
    this->scaleY = scaleY;
    this->side = side;
}


void Object::setRgb(int r,int g,int b){
    this->Red = r;
    this->Green = g;
    this->Blue = b;
}

void Object::setRedColor(int r){
    this->Red = r;
}

void Object::setGreenColor(int g){
    this->Green = g;
}

void Object::setBlueColor(int b){
    this->Blue = b;
}

int Object::getRed(){
    return this->Red;
}

int Object::getGreen(){
    return this->Green;
}

int Object::getBlue(){
    return this->Blue;
}

void Object::setType(int type){
    this->type = type;
}

int Object::getType(){
    return this->type;
}

void Object::setScaleX(double n){
    this->scaleX = n;
}

double Object::getScaleX(){
    return this->scaleX;
}

void Object::setScaleY(double n){
    this->scaleY = n;
}

double Object::getScaleY(){
    return this->scaleY;
}

void Object::setSide(int n){
    this->side = n;
}

int Object::getSide(){
    return this->side;
}

