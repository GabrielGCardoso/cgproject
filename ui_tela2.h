/********************************************************************************
** Form generated from reading UI file 'tela2.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TELA2_H
#define UI_TELA2_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_tela2
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QFrame *line;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QSpinBox *spinBox;
    QSpacerItem *horizontalSpacer;
    QFrame *line_2;
    QPushButton *pushButton;

    void setupUi(QWidget *tela2)
    {
        if (tela2->objectName().isEmpty())
            tela2->setObjectName(QString::fromUtf8("tela2"));
        tela2->resize(264, 210);
        verticalLayout_3 = new QVBoxLayout(tela2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(tela2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);


        verticalLayout_3->addLayout(verticalLayout);

        line = new QFrame(tela2);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(tela2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);


        verticalLayout_3->addLayout(verticalLayout_2);

        spinBox = new QSpinBox(tela2);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMinimum(5);

        verticalLayout_3->addWidget(spinBox);

        horizontalSpacer = new QSpacerItem(243, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        verticalLayout_3->addItem(horizontalSpacer);

        line_2 = new QFrame(tela2);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line_2);

        pushButton = new QPushButton(tela2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_3->addWidget(pushButton);


        retranslateUi(tela2);
        QObject::connect(pushButton, SIGNAL(clicked()), tela2, SLOT(trocarTela()));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), tela2, SLOT(tamanhoDaMalha(int)));

        QMetaObject::connectSlotsByName(tela2);
    } // setupUi

    void retranslateUi(QWidget *tela2)
    {
        tela2->setWindowTitle(QApplication::translate("tela2", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("tela2", "Bem vindo!", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("tela2", "Digite o tamanho da Malha :", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("tela2", "Continuar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class tela2: public Ui_tela2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TELA2_H
