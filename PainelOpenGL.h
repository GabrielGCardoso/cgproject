#ifndef PAINELOPENGL_H
#define PAINELOPENGL_H

#include <QWidget>
#include <QGLWidget>
#include <vector>
#include "Object.h"

class PainelOpenGL : public QGLWidget
{
    Q_OBJECT
public slots:
    void desenhar(int type);
    void triangulo();
    void quadrado();
    void rules();
    void alterarMalha(int n);
    void retaOriginal();
    void setX1(int n);
    void setY1(int n);
    void setX2(int n);
    void setY2(int n);
    void desenharPixel();
    void redColorChange(int n);
    void greenColorChange(int n);
    void blueColorChange(int n);
    void modificarEscalaX(double n);
    void modificarEscalaY(double n);
public:
    explicit PainelOpenGL(QWidget *parent = 0);


protected:
    void initializeGL();
    void resizeGL(int width,int height);
    void paintGL();

signals:

private:
    int tipo;
    int TelaX,TelaY;
    float lado;
    int x1,x2,y1,y2;
    bool grid; //boolean grid;
    bool lineView; //boolean lineView;
    Object *o;
    std::vector<Object> Objetos;
};

#endif // PAINELOPENGL_H
