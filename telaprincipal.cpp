#include "telaprincipal.h"
#include "ui_telaprincipal.h"
#include <QPalette>

TelaPrincipal::TelaPrincipal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TelaPrincipal)
{
    ui->setupUi(this);
    connect(ui->cb_Objetos,SIGNAL(currentIndexChanged(int)),this,SLOT(comboBoxIndexChanged()));
//    connect(ui->RedSlider,SIGNAL(valueChanged(int)),SLOT(onColorChanged()));
//    connect(ui->GreenSlider,SIGNAL(valueChanged(int)),SLOT(onColorChanged()));
//    connect(ui->BlueSlider,SIGNAL(valueChanged(int)),SLOT(onColorChanged()));
//    onColorChanged();
}

TelaPrincipal::~TelaPrincipal()
{
    delete ui;
}

void TelaPrincipal::comboBoxIndexChanged(){
    if( ui->cb_Objetos->currentIndex() == 3){
        ui->sb_Lados->setEnabled( true );
    }
    else{
        ui->sb_Lados->setEnabled( false );
    }
}

