#-------------------------------------------------
#
# Project created by QtCreator 2018-08-25T00:06:10
#
#-------------------------------------------------

QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
LIBS += -lGL -lGLU #-lOpengl32
TARGET = testeProjeto
TEMPLATE = app


SOURCES += main.cpp\
        telaprincipal.cpp \
    PainelOpenGL.cpp \
    Object.cpp

HEADERS  += telaprincipal.h \
    PainelOpenGL.h \
    Object.h

FORMS    += telaprincipal.ui \

OTHER_FILES += \
    images/triangulo.png \
    images/quadrado.png
