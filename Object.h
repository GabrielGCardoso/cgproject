#ifndef OBJECT_H
#define OBJECT_H

class Object
{

public:
    Object();
    Object(int r,int g,int b,int t,double scaleX,double scaleY,int side);
    void setRgb(int r,int g,int b);
    void setRedColor(int r);
    void setGreenColor(int g);
    void setBlueColor(int b);
    void setType(int type);
    void setScaleX(double n);
    void setScaleY(double n);
    void setSide(int n);
    int getRed();
    int getGreen();
    int getBlue();
    double getScaleX();
    double getScaleY();
    int getType();
    int getSide();


private:
    int Red,Green,Blue;
    int type;
    float scaleX,scaleY;
    int side;
    double posX,posY;

};


#endif // OBJECT_H
