#ifndef TELAPRINCIPAL_H
#define TELAPRINCIPAL_H

#include <QMainWindow>
#include <QColor>

namespace Ui {
class TelaPrincipal;
}

class TelaPrincipal : public QMainWindow
{
    Q_OBJECT
//    Q_PROPERTY(QColor color READ color NOTIFY colorChanged)

public:
    TelaPrincipal(QWidget *parent = 0);
    ~TelaPrincipal();

//    QColor color() const
//    {
//        return m_color;
//    }

signals:
//    void colorChanged(QColor arq);

private slots:
//    void onColorChanged();
    void comboBoxIndexChanged();

private:
    Ui::TelaPrincipal *ui;
//    QColor m_color;
    int tam;
};

#endif // TELAPRINCIPAL_H
