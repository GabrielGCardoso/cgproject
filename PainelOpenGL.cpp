#include "PainelOpenGL.h"
#include <GL/glu.h>
#include <qsize.h>
#include <math.h>


PainelOpenGL::PainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{
    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);

    tipo = 1;
    grid = false;
    lineView = false;
    TelaX = 5;
    TelaY = 5;
    lado = 3;
    o = new Object();
    o->setRgb(0,0,0);
}

void PainelOpenGL::initializeGL(){
    glShadeModel(GL_SMOOTH);

    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void PainelOpenGL::resizeGL(int width, int height){
    double menorX = 0,menorY = 0;
        glViewport( 0, 0, (GLint)width, (GLint)height );

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluOrtho2D(menorX, TelaX, menorY, TelaY);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void PainelOpenGL::paintGL(){
    int i=0,j=0,cont,qtd;
    static float red,green,blue,scale;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity(); // limpa todas as transformações


    glTranslated(0.1, 0.1, 0.0);    
    if(grid){
        glLineWidth(1);
        glLineStipple(1, 0xAAAA );
        glEnable( GL_LINE_STIPPLE );
            glColor3f(0,0.5,1); // Define a cor de desenho: vermelho
            glBegin(GL_LINES);
                for(i = 0;i<TelaX; i++){
                    glVertex2f(0,i);
                    glVertex2f(TelaX,i);
                }
                for(j = 0;j<TelaY; j++){
                    glVertex2f(j,0);
                    glVertex2f(j,TelaY);
                }
            glEnd();
            glDisable(GL_LINE_STIPPLE );
    }

    for(std::vector<Object>::iterator obj = Objetos.begin();obj != Objetos.end(); obj++){
        glPushMatrix();
        tipo = obj->getType();
        red = (float)obj->getRed()/255;
        green = (float)obj->getGreen()/255;
        blue = (float)obj->getBlue()/255;
        glScalef((float)obj->getScaleX(),(float)obj->getScaleY(),1);


    if(tipo == 1){
        glLineWidth(0.1);
        glColor3f(0,0,0); // Define a cor de desenho: vermelho
        for(qtd = x1+1;qtd <= x2;qtd++){
        glBegin(GL_POLYGON);
        for(cont = 1; cont < 50; cont++){
            glVertex2f(cos(cont*2*3.14159265/50)/2 + (qtd-0.5),
                       sin(cont*2*3.14159265/50)/2 + (0.5));
        }
        glEnd();
        }

    }
    else if(tipo == 3){
        glLineWidth(1);
        glColor3f(red,green,blue); // Define a cor de desenho
        glBegin(GL_TRIANGLES);
            glVertex2f(0,0);
            glVertex2f(10,0);
            glVertex2f(5,5);
        glEnd();
    }
    else if(tipo == 4){
        glLineWidth(1);

        glColor3f(red,green,blue); // Define a cor de desenho
        glBegin(GL_QUADS);
            glVertex2f(0,0);
            glVertex2f(5,0);
            glVertex2f(5,5);
            glVertex2f(0,5);
        glEnd();
    }
    else if(tipo == 5){
        int lados = obj->getSide();
        int raio = obj->getScaleX();
        glLineWidth(1);

        glColor3f(red,green,blue);
        glBegin(GL_POLYGON);
            for (int i = 0; i < lados; ++i)
                glVertex2f(raio*cos(i*2*3.14159265/lados),
                           raio*sin(i*2*3.14159265/lados));
        glEnd();
    }
        glPopMatrix();
    }
    if(lineView){
        glLineWidth(10);
        glColor3f(1,0,0);
        glBegin(GL_LINES);
            glVertex2f(x1,y1);
            glVertex2f(x2,y2);
        glEnd();
    }
    // Desenha um polígono com N lados informado pelo usuário

}

void PainelOpenGL::desenhar(int type){
    if(type == 0){
        o->setType(1);
    }
    else if(type == 1){
        o->setType(3);
    }
    else if(type == 2){
        o->setType(4);
    }
    Objetos.push_back(*o);
    updateGL();

}

void PainelOpenGL::triangulo(){
    desenhar(1);
}

void PainelOpenGL::quadrado(){    
    desenhar(2);
}

void PainelOpenGL::rules(){
    if(grid){
        grid = false;
    }
    else{
        grid = true;
    }
    updateGL();
}

void PainelOpenGL::alterarMalha(int n){
    TelaX = TelaY = n-1;
    this->resizeGL(width(),height());
    updateGL();
}

void PainelOpenGL::retaOriginal(){
    if(lineView){
        lineView = false;
    }
    else{
        lineView = true;
    }
    updateGL();
}

void PainelOpenGL::setX1(int n){
    x1 = n;
    updateGL();
}

void PainelOpenGL::setY1(int n){
    y1 = n;
    updateGL();
}

void PainelOpenGL::setX2(int n){
    x2 = n;
    updateGL();
}

void PainelOpenGL::setY2(int n){
    y2 = n;
    updateGL();
}

void PainelOpenGL::desenharPixel(){

}

void PainelOpenGL::redColorChange(int n){
    o->setRedColor(n);
    updateGL();
}

void PainelOpenGL::greenColorChange(int n){
    o->setGreenColor(n);
    updateGL();
}

void PainelOpenGL::blueColorChange(int n){
    o->setBlueColor(n);
    updateGL();
}

void PainelOpenGL::modificarEscalaX(double n){
    o->setScaleX(n);
    updateGL();
}

void PainelOpenGL::modificarEscalaY(double n){
    o->setScaleY(n);
    updateGL();
}
