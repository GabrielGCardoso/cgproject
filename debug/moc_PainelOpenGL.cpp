/****************************************************************************
** Meta object code from reading C++ file 'PainelOpenGL.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../PainelOpenGL.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PainelOpenGL.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PainelOpenGL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   14,   13,   13, 0x0a,
      33,   13,   13,   13, 0x0a,
      45,   13,   13,   13, 0x0a,
      56,   13,   13,   13, 0x0a,
      66,   64,   13,   13, 0x0a,
      84,   13,   13,   13, 0x0a,
      99,   64,   13,   13, 0x0a,
     110,   64,   13,   13, 0x0a,
     121,   64,   13,   13, 0x0a,
     132,   64,   13,   13, 0x0a,
     143,   13,   13,   13, 0x0a,
     159,   64,   13,   13, 0x0a,
     179,   64,   13,   13, 0x0a,
     201,   64,   13,   13, 0x0a,
     222,   64,   13,   13, 0x0a,
     247,   64,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PainelOpenGL[] = {
    "PainelOpenGL\0\0type\0desenhar(int)\0"
    "triangulo()\0quadrado()\0rules()\0n\0"
    "alterarMalha(int)\0retaOriginal()\0"
    "setX1(int)\0setY1(int)\0setX2(int)\0"
    "setY2(int)\0desenharPixel()\0"
    "redColorChange(int)\0greenColorChange(int)\0"
    "blueColorChange(int)\0modificarEscalaX(double)\0"
    "modificarEscalaY(double)\0"
};

void PainelOpenGL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PainelOpenGL *_t = static_cast<PainelOpenGL *>(_o);
        switch (_id) {
        case 0: _t->desenhar((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->triangulo(); break;
        case 2: _t->quadrado(); break;
        case 3: _t->rules(); break;
        case 4: _t->alterarMalha((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->retaOriginal(); break;
        case 6: _t->setX1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setY1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setX2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setY2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->desenharPixel(); break;
        case 11: _t->redColorChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->greenColorChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->blueColorChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->modificarEscalaX((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->modificarEscalaY((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PainelOpenGL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PainelOpenGL::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_PainelOpenGL,
      qt_meta_data_PainelOpenGL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PainelOpenGL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PainelOpenGL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PainelOpenGL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PainelOpenGL))
        return static_cast<void*>(const_cast< PainelOpenGL*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int PainelOpenGL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
