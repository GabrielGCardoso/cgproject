/********************************************************************************
** Form generated from reading UI file 'telaprincipal.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TELAPRINCIPAL_H
#define UI_TELAPRINCIPAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "PainelOpenGL.h"

QT_BEGIN_NAMESPACE

class Ui_TelaPrincipal
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_Objeto;
    QComboBox *cb_Objetos;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_4;
    QSpinBox *sb_Lados;
    QFrame *line_3;
    QVBoxLayout *verticalLayout_8;
    QPushButton *btn_desenhar;
    QVBoxLayout *verticalLayout;
    QLabel *lb_Malha;
    QSpinBox *sb_Malha;
    QVBoxLayout *verticalLayout_5;
    QFrame *line_2;
    QCheckBox *ckb_regua;
    QCheckBox *checkBox;
    QGridLayout *gridLayout;
    PainelOpenGL *PainelGL;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QLabel *label_3;
    QSlider *RedSlider;
    QSpinBox *RedSB;
    QFrame *line_4;
    QSpinBox *BlueSB;
    QSlider *BlueSlider;
    QLabel *label;
    QLabel *label_2;
    QSpinBox *GreenSB;
    QSlider *GreenSlider;
    QFrame *line;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_5;
    QDoubleSpinBox *sbScaleX;
    QLabel *label_6;
    QDoubleSpinBox *sbScaleY;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *TelaPrincipal)
    {
        if (TelaPrincipal->objectName().isEmpty())
            TelaPrincipal->setObjectName(QString::fromUtf8("TelaPrincipal"));
        TelaPrincipal->resize(732, 448);
        centralWidget = new QWidget(TelaPrincipal);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_4 = new QGridLayout(centralWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_Objeto = new QLabel(centralWidget);
        label_Objeto->setObjectName(QString::fromUtf8("label_Objeto"));

        verticalLayout_2->addWidget(label_Objeto);

        cb_Objetos = new QComboBox(centralWidget);
        cb_Objetos->setObjectName(QString::fromUtf8("cb_Objetos"));

        verticalLayout_2->addWidget(cb_Objetos);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_6->addWidget(label_4);

        sb_Lados = new QSpinBox(centralWidget);
        sb_Lados->setObjectName(QString::fromUtf8("sb_Lados"));
        sb_Lados->setEnabled(false);
        sb_Lados->setMinimum(3);
        sb_Lados->setMaximum(80);

        verticalLayout_6->addWidget(sb_Lados);


        verticalLayout_2->addLayout(verticalLayout_6);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line_3);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        btn_desenhar = new QPushButton(centralWidget);
        btn_desenhar->setObjectName(QString::fromUtf8("btn_desenhar"));

        verticalLayout_8->addWidget(btn_desenhar);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lb_Malha = new QLabel(centralWidget);
        lb_Malha->setObjectName(QString::fromUtf8("lb_Malha"));

        verticalLayout->addWidget(lb_Malha);

        sb_Malha = new QSpinBox(centralWidget);
        sb_Malha->setObjectName(QString::fromUtf8("sb_Malha"));
        sb_Malha->setMinimum(5);

        verticalLayout->addWidget(sb_Malha);


        verticalLayout_8->addLayout(verticalLayout);


        verticalLayout_2->addLayout(verticalLayout_8);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_5->addWidget(line_2);

        ckb_regua = new QCheckBox(centralWidget);
        ckb_regua->setObjectName(QString::fromUtf8("ckb_regua"));

        verticalLayout_5->addWidget(ckb_regua);

        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout_5->addWidget(checkBox);


        verticalLayout_2->addLayout(verticalLayout_5);


        gridLayout_5->addLayout(verticalLayout_2, 0, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_5, 0, 3, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PainelGL = new PainelOpenGL(centralWidget);
        PainelGL->setObjectName(QString::fromUtf8("PainelGL"));
        PainelGL->setMinimumSize(QSize(500, 200));

        gridLayout->addWidget(PainelGL, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout, 0, 0, 2, 1);

        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        RedSlider = new QSlider(widget);
        RedSlider->setObjectName(QString::fromUtf8("RedSlider"));
        RedSlider->setMaximum(255);
        RedSlider->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(RedSlider, 0, 1, 1, 1);

        RedSB = new QSpinBox(widget);
        RedSB->setObjectName(QString::fromUtf8("RedSB"));
        RedSB->setMaximum(255);

        gridLayout_2->addWidget(RedSB, 0, 2, 1, 1);

        line_4 = new QFrame(widget);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_4, 0, 3, 3, 1);

        BlueSB = new QSpinBox(widget);
        BlueSB->setObjectName(QString::fromUtf8("BlueSB"));
        BlueSB->setMaximum(255);

        gridLayout_2->addWidget(BlueSB, 2, 2, 1, 1);

        BlueSlider = new QSlider(widget);
        BlueSlider->setObjectName(QString::fromUtf8("BlueSlider"));
        BlueSlider->setMaximum(255);
        BlueSlider->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(BlueSlider, 2, 1, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        GreenSB = new QSpinBox(widget);
        GreenSB->setObjectName(QString::fromUtf8("GreenSB"));
        GreenSB->setMaximum(255);

        gridLayout_2->addWidget(GreenSB, 1, 2, 1, 1);

        GreenSlider = new QSlider(widget);
        GreenSlider->setObjectName(QString::fromUtf8("GreenSlider"));
        GreenSlider->setMaximum(255);
        GreenSlider->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(GreenSlider, 1, 1, 1, 1);


        gridLayout_4->addWidget(widget, 1, 2, 1, 3);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line, 0, 1, 2, 2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_3->addWidget(label_5);

        sbScaleX = new QDoubleSpinBox(centralWidget);
        sbScaleX->setObjectName(QString::fromUtf8("sbScaleX"));

        verticalLayout_3->addWidget(sbScaleX);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_3->addWidget(label_6);

        sbScaleY = new QDoubleSpinBox(centralWidget);
        sbScaleY->setObjectName(QString::fromUtf8("sbScaleY"));

        verticalLayout_3->addWidget(sbScaleY);


        gridLayout_4->addLayout(verticalLayout_3, 0, 4, 1, 1);

        TelaPrincipal->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(TelaPrincipal);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 732, 21));
        TelaPrincipal->setMenuBar(menuBar);
        mainToolBar = new QToolBar(TelaPrincipal);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        TelaPrincipal->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(TelaPrincipal);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        TelaPrincipal->setStatusBar(statusBar);

        retranslateUi(TelaPrincipal);
        QObject::connect(cb_Objetos, SIGNAL(currentIndexChanged(int)), PainelGL, SLOT(desenhar(int)));
        QObject::connect(ckb_regua, SIGNAL(stateChanged(int)), PainelGL, SLOT(rules()));
        QObject::connect(sb_Malha, SIGNAL(valueChanged(int)), PainelGL, SLOT(alterarMalha(int)));
        QObject::connect(checkBox, SIGNAL(stateChanged(int)), PainelGL, SLOT(retaOriginal()));
        QObject::connect(RedSlider, SIGNAL(valueChanged(int)), RedSB, SLOT(setValue(int)));
        QObject::connect(RedSB, SIGNAL(valueChanged(int)), RedSlider, SLOT(setValue(int)));
        QObject::connect(GreenSlider, SIGNAL(sliderMoved(int)), GreenSB, SLOT(setValue(int)));
        QObject::connect(GreenSB, SIGNAL(valueChanged(int)), GreenSlider, SLOT(setValue(int)));
        QObject::connect(BlueSlider, SIGNAL(sliderMoved(int)), BlueSB, SLOT(setValue(int)));
        QObject::connect(BlueSB, SIGNAL(valueChanged(int)), BlueSlider, SLOT(setValue(int)));
        QObject::connect(RedSB, SIGNAL(valueChanged(int)), PainelGL, SLOT(redColorChange(int)));
        QObject::connect(GreenSB, SIGNAL(valueChanged(int)), PainelGL, SLOT(greenColorChange(int)));
        QObject::connect(BlueSB, SIGNAL(valueChanged(int)), PainelGL, SLOT(blueColorChange(int)));
        QObject::connect(sbScaleX, SIGNAL(valueChanged(double)), PainelGL, SLOT(modificarEscalaX(double)));
        QObject::connect(sbScaleY, SIGNAL(valueChanged(double)), PainelGL, SLOT(modificarEscalaY(double)));

        QMetaObject::connectSlotsByName(TelaPrincipal);
    } // setupUi

    void retranslateUi(QMainWindow *TelaPrincipal)
    {
        TelaPrincipal->setWindowTitle(QApplication::translate("TelaPrincipal", "TelaPrincipal", 0, QApplication::UnicodeUTF8));
        label_Objeto->setText(QApplication::translate("TelaPrincipal", "Objeto :", 0, QApplication::UnicodeUTF8));
        cb_Objetos->clear();
        cb_Objetos->insertItems(0, QStringList()
         << QApplication::translate("TelaPrincipal", "Linha", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TelaPrincipal", "Triangulo", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TelaPrincipal", "Quadrado", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("TelaPrincipal", "Poligono", 0, QApplication::UnicodeUTF8)
        );
        label_4->setText(QApplication::translate("TelaPrincipal", "Lados", 0, QApplication::UnicodeUTF8));
        btn_desenhar->setText(QApplication::translate("TelaPrincipal", "Desenhar", 0, QApplication::UnicodeUTF8));
        lb_Malha->setText(QApplication::translate("TelaPrincipal", "Malha", 0, QApplication::UnicodeUTF8));
        ckb_regua->setText(QApplication::translate("TelaPrincipal", "Regua", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("TelaPrincipal", "Reta Original", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("TelaPrincipal", "Blue", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TelaPrincipal", "Red", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("TelaPrincipal", "Green", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("TelaPrincipal", "Escala X :", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("TelaPrincipal", "Escala Y :", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TelaPrincipal: public Ui_TelaPrincipal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TELAPRINCIPAL_H
